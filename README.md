# MongoDB2MySQL
自动将MongoDB中的数据转换到MySQL中,并且遇到列表数据会自动创建新的关联关系表

---

首先感谢作者的贡献，解决了我的问题！

使用这个工具的目的是为了将`MongoDB`的数据模型和表结构导入到`PowerDesigner`，但是因为`MongoDB`是`NoSQL`，没有依赖，所以我就折中了一下，先转换成`MySQL`再导入到`PowerDesigner`，最终达到了目的。

再次对作者表示感谢！

---

## 准备工作：
1. 选择使用脚本：[MongoDB2MySQL](https://gitee.com/dekin/MongoDB2MySQL)

2. 准备环境：我去阿里云上开了一台`4H 4G`的`抢占式实例`，用完就可以删除掉。转换过程中大致资源占用情况如下：

![资源占用情况](https://images.gitee.com/uploads/images/2019/1106/230948_b14a0dac_1119777.png)

根据自己实际情况斟酌，配置低一些可能数据量大的时候转换比较慢。

### 安装MySQL

[安装MySQL](https://www.cnblogs.com/caoxb/p/9405323.html)

### 安装MongoDB

### 安装脚本使用的Python依赖包：

代码里使用的python版本是`python2`，`numpy`和`pandas`支持2.7，不能安装最新版，要安装低版本：

`pip install numpy==1.16.0 pandas==0.23.0`

---

`pip install MySQL-Python`

遇到错误：
```
EnvironmentError: mysql_config not found
    ----------------------------------------
ERROR: Command errored out with exit status 1: python setup.py egg_info Check the logs for full command output.
```

解决方法：安装`mysql-devel`，`yum install mysql-devel -y`，如果还是不行，参考[https://www.cnblogs.com/dupengcheng/p/8626512.html](https://www.cnblogs.com/dupengcheng/p/8626512.html)尝试一下。

`pip install pymongo ConfigParser tqdm threadpool`

## 根据自己情况修改配置

貌似一次只能转换一张表。

转换过程中出现错误：

```
[root@jh-test src]# python main.py 
  0%|          | 0/1723811 [00:00<?, ?it/s]Traceback (most recent call last):
  File "main.py", line 148, in <module>
    disposeData(data, mysql_conn, doList=list_to_new_table)
  File "main.py", line 77, in disposeData
    conn.executeInsterSQL(mongodb_collection, key_arr, pla_arr, val_arr)
  File "/root/src/DBUtils.py", line 109, in executeInsterSQL
    cursor.execute(sql, val_arr)
  File "/usr/lib64/python2.7/site-packages/MySQLdb/cursors.py", line 205, in execute
    self.errorhandler(self, exc, value)
  File "/usr/lib64/python2.7/site-packages/MySQLdb/connections.py", line 36, in defaulterrorhandler
    raise errorclass, errorvalue
_mysql_exceptions.DataError: (1406, "Data too long for column 'response' at row 1")
  0%|          | 0/1723811 [00:00<?, ?it/s]
```

可能是这个`Collection`中`response`这一列数据字符串太长了，先转换其他`Collection`

我的`Python`只是入门，就不去改代码了。但是因为我想要的是表结构，没有数据也是没有关系的，就暂时放过，有空再说。

`PowerDesigner`使用`ODBC`连接`MySQL`生成数据库模型，[教程](https://blog.csdn.net/transformer_WSZ/article/details/78011092)

按照文中的一步一步来就行，其中需要注意的是**`ODBC`选择安装32位**的，可能是因为`powerdesigner`是32位的。

最终成功达到目的，但是因为是`MongoDB`导出的`MySQL`的原因，模型是没有任何依赖关系的，只能自己添加了。

